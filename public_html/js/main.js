var options1 = {
  series: [44, 11, 32, 55, 15],
  labels: ["Тут какой-то текст про нарушения про нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения", "Тут какой-то текст про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения", "Тут какой-то текст про нарушения", "Тут какой-то текст про нарушения", "Тут какой-то текст про нарушения Тут какой-то текст про нарушения Тут какой-то текст про нарушения Тут какой-то текст про нарушения Тут какой-то текст про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения нарушения про нарушения про нарушения нарушения про нарушения нарушения про нарушения"],
  chart: {
    width: '100%',
    type: 'donut',
  },
  dataLabels: {
    enabled: false
  },
  legend: {
    width: '100%',
    formatter: function(val, opts) {
      return opts.w.globals.series[opts.seriesIndex] + ' ед.' + " - " + val
    },
    fontSize: '16px'
  },
  colors: ['#040A29', '#364FEE', '#FC1050', '#36EBDF', '#8B92A8'],
  responsive: [
    {
      breakpoint: 991,
      options: {
        legend: {
          position: 'bottom'
        },
        chart: {
          width: 500
        }
      }
    },
    {
      breakpoint: 767,
      options: {
        legend: {
          position: 'bottom'
        },
        chart: {
          width: '100%',
          height: 400
        }
      }
    }
  ]
}

var options2 = {
  series: [44, 55, 41, 17, 15],
  labels: ["Тут какой-то текст про нарушения", "Тут какой-то текст про нарушения", "Тут какой-то текст про нарушения", "Тут какой-то текст про нарушения", "Тут какой-то текст про нарушения"],
  chart: {
    width: '100%',
    type: 'donut',
  },
  dataLabels: {
    enabled: false
  },
  legend: {
    width: '100%',
    formatter: function(val, opts) {
      return opts.w.globals.series[opts.seriesIndex] + ' ед.' + " - " + val
    },
    fontSize: '16px'
  },
  colors: ['#040A29', '#364FEE', '#FC1050', '#36EBDF', '#8B92A8'],
  responsive: [
    {
      breakpoint: 991,
      options: {
        legend: {
          position: 'bottom'
        },
        chart: {
          width: 500
        }
      }
    },
    {
      breakpoint: 767,
      options: {
        legend: {
          position: 'bottom'
        },
        chart: {
          width: '100%',
          height: 400
        }
      }
    }
  ]
};

var options3 = {
  series: [{
    data: [21, 22, 10, 28]
  }],
    chart: {
    height: 350,
    type: 'bar'
  },
  colors: ['#040A29', '#364FEE', '#FC1050', '#36EBDF'],
  plotOptions: {
    bar: {
      columnWidth: '50px',
      distributed: true,
    }
  },
  dataLabels: {
    enabled: false
  },
  legend: {
    show: false
  },
  xaxis: {
    categories: [
      ['Предписания', '(представления)'],
      ['Информационное', 'письмо в', 'контрольные органы'],
      ['Предоставление', 'информации', 'правоохранительным', 'органам'],
      'Иное',
    ],
    labels: {
      rotate: 0,
      style: {
        colors: ['#040A29', '#364FEE', '#FC1050', '#36EBDF'],
        fontSize: '16px'
      }
    }
  }
};


function totalVal (arr) {
  return arr.series.map(i => x += i, x=0).reverse()[0]
}


var diagram3 = document.querySelector("#chart-3")
var chart3 = new ApexCharts(diagram3, options3);
chart3.render();

var diagram1 = document.querySelector("#chart-1")
var diagram2 = document.querySelector("#chart-2")

if (diagram1 && diagram2) {
  var chart1 = new ApexCharts(diagram1, options1);
  var chart2 = new ApexCharts(diagram2, options2);
  
  chart1.render();
  chart2.render();

  document.querySelector(".total-1").innerHTML = totalVal(options1)
  document.querySelector(".total-2").innerHTML = totalVal(options2)
}

(function () {
  'use strict';
  window.addEventListener('load', function () {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$(document).ready(function(){
  $('.dropdown-link').on('click', function() {
    $(this).siblings('.dropdown-menu').addClass('show')
  });
  $(document).mouseup(function (e) {
    var container = $(".dropdown");
    if (container.has(e.target).length === 0){
        $('.dropdown-menu').removeClass('show');
    }
  });
  $('.news-main').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: `<button class="slider-btn left-btn">
                  <svg width="16" height="23" viewBox="0 0 16 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.25 10.3047L10.8125 0.742188C11.1406 0.414062 11.5391 0.25 12.0078 0.25C12.4766 0.25 12.875 0.414062 13.2031 0.742188L14.75 2.35938C15.0781 2.6875 15.2422 3.08594 15.2422 3.55469C15.2422 4.02344 15.0781 4.39844 14.75 4.67969L8 11.5L14.75 18.25C15.0781 18.5781 15.2422 18.9766 15.2422 19.4453C15.2422 19.9141 15.0781 20.3125 14.75 20.6406L13.2031 22.2578C12.875 22.5859 12.4766 22.75 12.0078 22.75C11.5391 22.75 11.1406 22.5859 10.8125 22.2578L1.25 12.6953C0.921875 12.3672 0.757812 11.9688 0.757812 11.5C0.757812 11.0312 0.921875 10.6328 1.25 10.3047Z" fill="white"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="slider-btn right-btn">
                  <svg width="16" height="23" viewBox="0 0 16 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.75 12.6953L5.1875 22.2578C4.85938 22.5859 4.46094 22.75 3.99219 22.75C3.52344 22.75 3.125 22.5859 2.79688 22.2578L1.25 20.6406C0.921875 20.3125 0.757812 19.9141 0.757812 19.4453C0.757812 18.9766 0.921875 18.5781 1.25 18.25L8 11.5L1.25 4.75C0.921875 4.42187 0.757812 4.02344 0.757812 3.55469C0.757812 3.08594 0.921875 2.6875 1.25 2.35938L2.79688 0.742188C3.125 0.414062 3.52344 0.25 3.99219 0.25C4.46094 0.25 4.85938 0.414062 5.1875 0.742188L14.75 10.3047C15.0781 10.6328 15.2422 11.0312 15.2422 11.5C15.2422 11.9688 15.0781 12.3672 14.75 12.6953Z" fill="white"/>
                  </svg>
                </button>`,
    dots: true
  });
  $('.banners__slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: `<button class="slider-btn left-btn">
                  <svg width="16" height="23" viewBox="0 0 16 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.25 10.3047L10.8125 0.742188C11.1406 0.414062 11.5391 0.25 12.0078 0.25C12.4766 0.25 12.875 0.414062 13.2031 0.742188L14.75 2.35938C15.0781 2.6875 15.2422 3.08594 15.2422 3.55469C15.2422 4.02344 15.0781 4.39844 14.75 4.67969L8 11.5L14.75 18.25C15.0781 18.5781 15.2422 18.9766 15.2422 19.4453C15.2422 19.9141 15.0781 20.3125 14.75 20.6406L13.2031 22.2578C12.875 22.5859 12.4766 22.75 12.0078 22.75C11.5391 22.75 11.1406 22.5859 10.8125 22.2578L1.25 12.6953C0.921875 12.3672 0.757812 11.9688 0.757812 11.5C0.757812 11.0312 0.921875 10.6328 1.25 10.3047Z" fill="white"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="slider-btn right-btn">
                  <svg width="16" height="23" viewBox="0 0 16 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.75 12.6953L5.1875 22.2578C4.85938 22.5859 4.46094 22.75 3.99219 22.75C3.52344 22.75 3.125 22.5859 2.79688 22.2578L1.25 20.6406C0.921875 20.3125 0.757812 19.9141 0.757812 19.4453C0.757812 18.9766 0.921875 18.5781 1.25 18.25L8 11.5L1.25 4.75C0.921875 4.42187 0.757812 4.02344 0.757812 3.55469C0.757812 3.08594 0.921875 2.6875 1.25 2.35938L2.79688 0.742188C3.125 0.414062 3.52344 0.25 3.99219 0.25C4.46094 0.25 4.85938 0.414062 5.1875 0.742188L14.75 10.3047C15.0781 10.6328 15.2422 11.0312 15.2422 11.5C15.2422 11.9688 15.0781 12.3672 14.75 12.6953Z" fill="white"/>
                  </svg>
                </button>`,
    responsive: [
      {
        breakpoint: 991,
        settings: 'unslick'
      }
    ]
  });
});