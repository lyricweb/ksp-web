"use strict";

var App = function () {

  function init() {
    svg4everybody();
  }

  function isTouch() {
    return ('ontouchstart' in document.documentElement) ? true : false;
  }

  function loadScript(url, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (script.readyState === "loaded" ||
          script.readyState === "complete") {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = function () {
        callback();
      };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  }

  document.addEventListener("DOMContentLoaded", function () {
    init();
  }, false);

  return {
    isTouch: isTouch,
    loadScript: loadScript
  };
}();